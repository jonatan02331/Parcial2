<br>
<?php
if(isset($_GET["year"])){
    $ajax = ($_GET["year"]);
}else{
    $ajax=0;
}
?><div class="row mt-3">
	<div class="col-lg-4"></div>
	<div class="col-lg-4 text-center">
		<div class="form-group">
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<label class="input-group-text">Edition</label>
				</div>
				<select class="form-select" id="year">
					<option value="0" <?php if( $ajax == 3) echo "selected"?>>Select edition</option>
					<option value="3" <?php if( $ajax == 3) echo "selected"?>>2020</option>
					<option value="2" <?php if( $ajax == 2) echo "selected"?>>2019</option>
					<option value="1" <?php if( $ajax == 1) echo "selected"?>>2018</option>
				</select>
			</div>
		</div>
	</div>	
</div>
<div id="result"></div>
<script>
$("#year").change(function(){
      if($("#year").val()!=0){		
      $("#result").html("<div class='text-center'><img src='img/loading2.gif'></div>");
			var year= $("#year").val();
		var url = "index.php?pid=<?php echo base64_encode("presentacion/stats.php") ?>&year=" + year;
		location.replace(url);
		}else{
		var url = "index.php?pid=<?php echo base64_encode("presentacion/inicio.php") ?>&year=" + year;
		location.replace(url);
		}
	});
</script>