<?php
include 'presentacion/inicio.php';
if(isset($_GET["year"])){
    $e = new editiontopic($_GET["year"]);
    $papers = $e -> Grafica1();
    $ptopic = $e -> Grafica2();
}
?>

<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Estadisticas</h5>
				<div class="card-body ">
					<div id="papers" style="width: 1000px; height: 300px;"></div>
					<div id="Ptopic" style="width: 1000px; height: 300px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
      google.charts.load("current", {packages:['corechart','bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
                  var data2 = google.visualization.arrayToDataTable([
          ['Year', 'Accepted', 'Rejected'],
<?php 
        foreach ($ptopic as $p){
            echo "['" . $p[0] . "', " . $p[1] .", " . $p[2]. "],\n";        
        }       
        ?>
        ]);

        var options2 = {
          chart: {
            title: 'Topic papers',
            
          }
        };

        var chart2 = new google.charts.Bar(document.getElementById('Ptopic'));

        chart2.draw(data2, google.charts.Bar.convertOptions(options2));
      
        var dataAccepted = google.visualization.arrayToDataTable([
          ['Accepted', 'Rejected'],
<?php 
        foreach ($papers as $p){
            echo "['" . $p[0] . "', " . $p[1] . "],\n";        
        }       
        ?>
        ]);
        var view = new google.visualization.DataView(dataAccepted);
      
      var options = {
        title: "Papers Accepted",
        is3D: true,
        bar: {groupWidth: "95%"},
        legend: { position: "right" },
    };

        var chartP = new google.visualization.PieChart(document.getElementById('papers'));
        chartP.draw(view, options);
        

        }
    </script>
