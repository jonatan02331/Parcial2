<?php
require_once 'persistencia/conexion.php';
require_once 'persistencia/editiontopicDAO.php';
class editiontopic{
    
    private $ided;
    private $conexion;
    private $editionTopicDAO;
    public function getIded(){
        return $this -> id;
    }
    public function editiontopic($id=""){
        
        $this -> ided =$id;
        $this -> conexion = new conexion();
        $this -> editionTopicDAO = new editiontopicDAO($id);
        
    }
    public function Grafica1(){
        $this -> conexion -> Abrir();
        $this -> conexion -> ejecutar($this -> editionTopicDAO -> Grafica1());
        $Papers = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Papers, $resultado);
        }
        $this -> conexion -> cerrar();
        return $Papers;
    }
    
    public function Grafica2(){
        $this -> conexion -> Abrir();
        $this -> conexion -> ejecutar($this -> editionTopicDAO -> Grafica2());
        $Papers = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($Papers, $resultado);
        }
        $this -> conexion -> cerrar();
        return $Papers;
    }
    
    
}